package com.nicholas.hospital.app;

import com.nicholas.hospital.doctors.DoctorsConsole;
import com.nicholas.hospital.section.SectionConsole;

import java.util.Scanner;

public class MainConsole {

    SectionConsole sectionConsole = new SectionConsole();
    DoctorsConsole doctorsConsole = new DoctorsConsole();


    public void runApplication() {

        System.out.println("Welcom to our hospital application!");

        boolean test = true;

        while (test) {

            System.out.println("Choose option: ");
            System.out.println("\t1. Sections");
            System.out.println("\t2. Doctors");
            System.out.println("\t3. Exit ");

            Scanner sc = new Scanner(System.in);
            int option = sc.nextInt();

            switch (option) {
                case 1:
                    sectionConsole.sectionConsoleOptions();
                    break;
                case 2:
                    doctorsConsole.doctorsConsoleOptions();
                    break;
                case 3:
                    test = false;
                    break;

                default:
                    System.out.println("The selected option doesn`t exist! ");
                    System.out.println();
            }

        }
    }


}
