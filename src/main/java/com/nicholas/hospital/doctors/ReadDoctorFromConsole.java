package com.nicholas.hospital.doctors;

import java.util.Scanner;

public class ReadDoctorFromConsole {

    public Doctor readDoctorFromConsole (Scanner scanner){
        Doctor doctor = new Doctor();

        System.out.println("Please insert doctor`s firt name ");
        scanner.nextLine();
        String name = scanner.nextLine();
        doctor.setFirstName(name);

        System.out.println("Please insert doctor`s last name ");
        String lastName = scanner.nextLine();
        doctor.setLastName(lastName);

        System.out.println("Plase insert doctors`s salary ");
        int salary = scanner.nextInt();
        doctor.setSalary(salary);

//        System.out.println("Plase insert doctors`s specialization");
//        scanner.nextLine();
//        String specialization = scanner.nextLine();
//        doctor.setSpecialization(specialization);

        return doctor;

    }

}
