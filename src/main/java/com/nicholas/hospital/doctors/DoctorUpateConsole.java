package com.nicholas.hospital.doctors;

import java.util.Scanner;

public class DoctorUpateConsole {

    private DoctorsStore doctorsStore;

    public DoctorUpateConsole() {
        this.doctorsStore = DoctorsStore.getSingleInstance();
    }

    public void updateConsoleOptions(Scanner scanner) {

        System.out.println();
        boolean test = true;

        while (test) {

            System.out.println("Doctor update option was selected");
            System.out.println(" ");
            System.out.println("What do you want to change ");
            System.out.println("\t1. ID");
            System.out.println("\t2. First name");
            System.out.println("\t3. Last name ");
            System.out.println("\t4. Salary ");
            System.out.println("\t5. Specialization");
            System.out.println("\t6. Exit");


            int option = scanner.nextInt();

            switch (option) {
                case 1:
                    updateID(scanner);
                    break;
                case 2:
                    updateFirtName(scanner);
                    break;
                case 3:
                    updateLastName(scanner);
                    break;
                case 4:
                    updateSalary(scanner);
                    break;
                case 5:
                    updateSpecialization(scanner);
                    break;
                case 6:
                    test = false;
                    break;
                default:
                    System.out.println("The selected option doesn`t exist! ");
            }
        }
    }

    public void updateID(Scanner scanner) {
        doctorsStore.setNewId(scanner);
    }

    public void updateFirtName(Scanner scanner) {
        doctorsStore.setNewFirstName(scanner);
    }

    public void updateLastName(Scanner scanner) {
        doctorsStore.setNewLastName(scanner);
    }

    public void updateSalary(Scanner scanner) {
        doctorsStore.setNewSalary(scanner);
    }

    public void updateSpecialization(Scanner scanner) {
        doctorsStore.setNewSpecialization(scanner);
    }


}
