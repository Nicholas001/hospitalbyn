package com.nicholas.hospital.doctors;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.nicholas.hospital.utils.Paths;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ReadDoctorFromFile {


    public List<Doctor> readDoctorsFromTxtFile() {

        /**
         Metoda functionala prin care sunt preluate date din fisierul txt
         */

        List<Doctor> doctors = new LinkedList<>();

        try {
            FileReader fileReader = new FileReader(Paths.PATH_TO_DOCTORS_TXT_FILE);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = bufferedReader.readLine();

            while (line != null) {
                String[] elements = line.split(" ");
                Doctor doctor = new Doctor();
                doctor.setId(Integer.parseInt(elements[0]));
                doctor.setFirstName(elements[1]);
                doctor.setLastName(elements[2]);
                doctor.setSalary(Integer.parseInt(elements[3]));
                doctor.setSpecialization(elements[4]);
                doctors.add(doctor);
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return doctors;
    }


    public List<Doctor> readDoctorsFromJsonUsingGson() {
        List<Doctor> list = new LinkedList<>();
        try {
            Gson gson = new Gson();
            FileReader fileReader = new FileReader(Paths.PATH_TO_DOCTORS_JSON_FILE);
            JsonReader reader = new JsonReader(fileReader);
            Doctor[] doctors = gson.fromJson(reader, Doctor[].class);
            list.addAll(Arrays.asList(doctors));
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        return list;
    }


    public List<Doctor> readDoctorsFromJsonUsingJSONParser() {

        /**
         Metoda partial functionala
         */

        List<Doctor> list = new LinkedList<>();

        try {
            JSONParser parser = new JSONParser();
            JSONArray jsonArray = (JSONArray) parser.parse(new FileReader(Paths.PATH_TO_DOCTORS_JSON_FILE));
            list = jsonArray;
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ParseException ex) {
            ex.getErrorType();
        }
        return list;
    }
}
