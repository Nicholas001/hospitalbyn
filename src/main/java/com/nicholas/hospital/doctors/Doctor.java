package com.nicholas.hospital.doctors;

public class Doctor {

    private int id;
    private String firstName;
    private String lastName;
    private int salary;
    private String specialization;


    public Doctor() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    @Override
    public String toString() {
        return "ID =" + id +
                ", Firt Name ='" + firstName + '\'' +
                ", Last Name ='" + lastName + '\'' +
                ", Salary =" + salary +
                ", Specialization ='" + specialization + '\'' +
                '\n';
    }
}
