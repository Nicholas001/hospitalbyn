package com.nicholas.hospital.doctors;

import com.google.gson.Gson;
import com.nicholas.hospital.section.Section;
import com.nicholas.hospital.section.SectionStore;
import com.nicholas.hospital.utils.Paths;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class DoctorsStore {

    private List<Doctor> allDoctors;
    private ReadDoctorFromFile readDoctorFromFile;
    private ReadDoctorFromConsole readDoctorFromConsole;
    private SectionStore sectionStore;
    private static DoctorsStore singleInstance;

    public DoctorsStore() {
        readDoctorFromFile = new ReadDoctorFromFile();
        allDoctors = readDoctorFromFile.readDoctorsFromJsonUsingGson();
        readDoctorFromConsole = new ReadDoctorFromConsole();
        this.sectionStore = SectionStore.getInstance();
    }

    public static DoctorsStore getSingleInstance() {
        if (singleInstance == null) {
            singleInstance = new DoctorsStore();
        }
        return singleInstance;
    }


    public List<Doctor> findAllDoctors() {
        return allDoctors;
    }

    public Doctor findDoctorbyId(Scanner scanner) {
        System.out.println("Please insert doctor ID: ");
        int id = scanner.nextInt();
        for (Doctor doctor : allDoctors) {
            if (doctor.getId() == id) {
                return doctor;
            }
        }
        return null;
    }

    public Doctor findDoctorBySpecialization(Scanner scanner) {
        System.out.println("Please insert doctor ID: ");
        String specialization = scanner.nextLine();
        for (Doctor doctor : allDoctors) {
            if (doctor.getSpecialization() == specialization) {
                return doctor;
            }
        }
        return null;
    }

    public void deleteAllDoctors() {
        allDoctors = new LinkedList<>();
        System.out.println("All doctors from list were deleted! ");
    }

    public void deleteDoctorById(Scanner scanner) {
        System.out.println("Please insert doctor ID: ");
        int id = scanner.nextInt();
        for (Iterator<Doctor> iterator = allDoctors.iterator(); iterator.hasNext(); ) {
            if (iterator.next().getId() == id) {
                iterator.remove();
                System.out.println("The operation was completed successfully");
            }
        }
    }


    public void insertNewDoctor(Scanner scanner) {
        Doctor doctor = readDoctorFromConsole.readDoctorFromConsole(scanner);
        System.out.println("Please select doctor specialization from the list :");
        Map<Integer, String> specialization = new HashMap<>();
        int x = 1;
        for (Section section : sectionStore.findAllSections()) {
            System.out.println(x + " " + section.getName());
            specialization.put(x, section.getName());
            x++;
        }
        int index = scanner.nextInt();
        doctor.setSpecialization(specialization.get(index));
        System.out.println();
        doctor.setId(generateId());
        allDoctors.add(doctor);
    }


    public void saveAndExitUsingTxtFile() {

        try {
            FileWriter fileWriter = new FileWriter(Paths.PATH_TO_DOCTORS_TXT_FILE);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            for (int i = 0; i < allDoctors.size(); i++) {
                String lineText = String.valueOf(allDoctors.get(i).getId()) + " " + allDoctors.get(i).getFirstName() + " " + allDoctors.get(i).getLastName() + " " + String.valueOf(allDoctors.get(i).getSalary()) + " " + allDoctors.get(i).getSpecialization() + '\n';
                bufferedWriter.write(lineText);
            }
            bufferedWriter.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void saveAndExitUsingJsonFile() {

        try {
            Gson gson = new Gson();
            FileWriter fileWriter = new FileWriter(Paths.PATH_TO_DOCTORS_JSON_FILE);

            fileWriter.write("[");
            for (Doctor each : allDoctors) {
                String json = gson.toJson(each);
                fileWriter.write(json);
                fileWriter.write(",");
            }
            fileWriter.write("]");
            fileWriter.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    public void setNewId(Scanner scanner) {
        System.out.println("Please insert Doctor ID ");
        int id = scanner.nextInt();
        System.out.println("Please insert the new ID ");
        int newId = scanner.nextInt();

        for (Doctor doctor : allDoctors) {
            if (doctor.getId() == id) {
                doctor.setId(newId);
                break;
            }
        }
        System.out.println("The new ID was set successfully");
        System.out.println();
    }

    public void setNewFirstName(Scanner scanner) {
        System.out.println("Please insert Doctor ID ");
        int id = scanner.nextInt();
        System.out.println("Please insert the new firt name ");
        String newName = scanner.nextLine();

        for (Doctor doctor : allDoctors) {
            if (doctor.getId() == id) {
                doctor.setFirstName(newName);
                break;
            }
        }
        System.out.println("The new first name was set successfully");
        System.out.println();
    }

    public void setNewLastName(Scanner scanner) {
        System.out.println("Please insert Doctor ID ");
        int id = scanner.nextInt();
        System.out.println("Please insert the new last name ");
        String newLastName = scanner.nextLine();

        for (Doctor doctor : allDoctors) {
            if (doctor.getId() == id) {
                doctor.setLastName(newLastName);
                break;
            }
        }
        System.out.println("The new last name was set successfully");
        System.out.println();
    }

    public void setNewSalary(Scanner scanner) {
        System.out.println("Please insert Doctor ID ");
        int id = scanner.nextInt();
        System.out.println("Please insert the new salary ");
        int newSalary = scanner.nextInt();

        for (Doctor doctor : allDoctors) {
            if (doctor.getId() == id) {
                doctor.setSalary(newSalary);
                break;
            }
        }
        System.out.println("The new salary was set successfully");
        System.out.println();
    }

    public void setNewSpecialization(Scanner scanner) {
        System.out.println("Please insert Doctor ID ");
        int id = scanner.nextInt();

        for (Doctor doctor : allDoctors) {
            if (doctor.getId() == id) {
                System.out.println("Please select doctor specializations from the list :");
                Map<Integer, String> specialization = new HashMap<>();
                int x = 1;
                for (Section section : sectionStore.findAllSections()) {
                    System.out.println(x + " " + section.getName());
                    specialization.put(x, section.getName());
                    x++;
                }
                int index = scanner.nextInt();
                doctor.setSpecialization(specialization.get(index));
            }
        }
        System.out.println("The new specialization was set successfully");
        System.out.println();
    }


    private int generateId() {
        int id = 0;
        for (Doctor doctor : allDoctors) {
            if (doctor.getId() > id) {
                id = doctor.getId();
            }
        }
        return id + 1;
    }

}
