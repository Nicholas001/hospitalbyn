package com.nicholas.hospital.doctors;

import java.util.Scanner;

public class DoctorsConsole {

    private DoctorsStore doctorsStore;
    private DoctorUpateConsole doctorUpateConsole;

    public DoctorsConsole() {
        this.doctorsStore = DoctorsStore.getSingleInstance();
        this.doctorUpateConsole = new DoctorUpateConsole();
    }

    public void doctorsConsoleOptions() {

        boolean test = true;

        while (test) {

            System.out.println(" Doctors options :");
            System.out.println();
            System.out.println("\t1. Display all doctors ");
            System.out.println("\t2. Display doctor by ID ");
            System.out.println("\t3. Display doctor by specialization ");
            System.out.println("\t4. Delete all doctors ");
            System.out.println("\t5. Delete doctor by ID ");
            System.out.println("\t6. Update doctor ");
            System.out.println("\t7. Insert new doctor ");
            System.out.println("\t8. Save and exit ");
            System.out.println("\t0. Exit without saving");

            Scanner sc = new Scanner(System.in);
            int opt = sc.nextInt();

            switch (opt) {
                case 1:
                    displayAllDoctors();
                    break;
                case 2:
                    displayDoctorById(sc);
                    break;
                case 3:
                    displayDoctorBySpecialization(sc);
                case 4:
                    deleteAllDoctors();
                    break;
                case 5:
                    deleteDoctorById(sc);
                    break;
                case 6:
                    update(sc);
                    break;
                case 7:
                    insertNewDoctor(sc);
                    break;
                case 8:
                    saveJson();
                    test = false;
                    break;
                case 0:
                    test = false;
                    break;
                default:
                    System.out.println("Plase enter a valid option!");
                    break;
            }
        }
    }

    public void displayAllDoctors() {
        System.out.println(doctorsStore.findAllDoctors());
    }

    public void displayDoctorById(Scanner scanner) {
        System.out.println(doctorsStore.findDoctorbyId(scanner));
    }

    public void displayDoctorBySpecialization(Scanner scanner) {
        System.out.println(doctorsStore.findDoctorBySpecialization(scanner));
    }

    public void deleteAllDoctors() {
        doctorsStore.deleteAllDoctors();
    }

    public void deleteDoctorById(Scanner scanner) {
        doctorsStore.deleteDoctorById(scanner);
    }

    public void insertNewDoctor(Scanner scanner) {
        doctorsStore.insertNewDoctor(scanner);
    }

    public void update(Scanner scanner) {
        doctorUpateConsole.updateConsoleOptions(scanner);
    }

    public void saveTxt() {
        doctorsStore.saveAndExitUsingTxtFile();
    }

    public void saveJson() {
        doctorsStore.saveAndExitUsingJsonFile();
    }
}
