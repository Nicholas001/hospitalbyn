package com.nicholas.hospital.section;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.nicholas.hospital.doctors.Doctor;
import com.nicholas.hospital.utils.Paths;

import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ReadSectionFromFile {


    public List<Section> readFromJsonFileUsingGson() {

        List<Section> sections = new LinkedList<>();

        try {
            Gson gson = new Gson();
            FileReader fileReader = new FileReader(Paths.PATH_TO_SECTIONS_JSON_FILE);
            JsonReader jsonReader = new JsonReader(fileReader);
            Section[] sections1 = gson.fromJson(jsonReader, Section[].class);
            sections.addAll(Arrays.asList(sections1));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return sections;
    }


    public List<Section> readSectionsFromTxt() {

        List<Section> sections = new LinkedList<>();

        try {
            FileReader fileReader = new FileReader(Paths.PATH_TO_SECTIONS_TXT_FILE);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = bufferedReader.readLine();

            while (line != null) {
                String[] element = line.split(" ");
                Section section = new Section();
                section.setId(Integer.parseInt(element[0]));
                section.setName(element[1]);
                section.setBudget(Integer.parseInt(element[2]));
                section.setBedNumber(Integer.parseInt(element[3]));
                sections.add(section);
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }


        return sections;
    }
}
