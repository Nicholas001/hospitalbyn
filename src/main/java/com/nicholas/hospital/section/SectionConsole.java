package com.nicholas.hospital.section;

import java.util.Scanner;

public class SectionConsole {

    private SectionStore sectionStore;
    private ReadSeactionFromConsole readSeactionFromConsole;
    private SectionUpdateConsole sectionUpdateConsole;


    public SectionConsole() {
        this.sectionStore = SectionStore.getInstance();
        this.readSeactionFromConsole = new ReadSeactionFromConsole();
        this.sectionUpdateConsole = new SectionUpdateConsole();
    }

    public void sectionConsoleOptions() {

        boolean test = true;

        while (test) {
            System.out.println("Sections options:");
            System.out.println();
            System.out.println("\t1. Display all sections ");
            System.out.println("\t2. Display section by ID ");
            System.out.println("\t3. Delete all sections ");
            System.out.println("\t4. Delete section by ID ");
            System.out.println("\t5. Update section ");
            System.out.println("\t6. Insert new section ");
            System.out.println("\t7. Save and exit ");
            System.out.println("\t0. Exit without saving");
            System.out.println();
            System.out.println("Please insert your option ");

            Scanner sc = new Scanner(System.in);
            int op = sc.nextInt();

            switch (op) {
                case 1:
                    displayAllSections();
                    break;
                case 2:
                    displaySectionByID(sc);
                    break;
                case 3:
                    deleteAllSections();
                    break;
                case 4:
                    deleteSectionById(sc);
                    break;
                case 5:
                    sectionUpdateConsole.updateConsoleOptions(sc);
                    break;
                case 6:
                    inserNewSection(sc);
                    break;
                case 7:
                    saveAndExit();
                    test = false;
                    break;
                case 0:
                    test = false;
                    break;
                default:
                    System.out.println("Plase enter a valid option!");
                    break;
            }

        }
    }

    public void displayAllSections() {
        System.out.println(sectionStore.findAllSections());
    }

    public void displaySectionByID(Scanner scanner) {
        System.out.println("Plese enter the section ID");
        int id = scanner.nextInt();
        System.out.println(sectionStore.findSectionById(id));
    }

    public void deleteAllSections() {
        sectionStore.deleteAllSections();
    }

    public void deleteSectionById(Scanner scanner) {
        System.out.println("Please insert the section ID");
        int id = scanner.nextInt();
        sectionStore.deleteSectionById(id);
    }

    public void inserNewSection(Scanner scanner) {
        sectionStore.insertNewSectionFromConsole(scanner);
    }

    public void saveAndExit() {
        sectionStore.saveAndExitUsingJson();
    }

}
