package com.nicholas.hospital.section;

import java.util.Scanner;

public class SectionUpdateConsole {

    private SectionStore sectionStore;

    public SectionUpdateConsole() {
        this.sectionStore = SectionStore.getInstance();
    }

    public void updateConsoleOptions(Scanner scanner) {

        System.out.println();
        boolean test = true;

        while (test) {

            System.out.println("Section update option was selected");
            System.out.println(" ");
            System.out.println("What do you want to change ");
            System.out.println("\t1. ID");
            System.out.println("\t2. Name");
            System.out.println("\t3. Budget ");
            System.out.println("\t4. Beds number ");
            System.out.println("\t5. Return to previous menu");


            int option = scanner.nextInt();

            switch (option) {
                case 1:
                    changeId(scanner);
                    break;
                case 2:
                    changeName(scanner);
                    break;
                case 3:
                    changeBudget(scanner);
                    break;
                case 4:
                    changeBedsNumber(scanner);
                    break;
                case 5:
                    test = false;
                    break;
                default:
                    System.out.println("The selected option doesn`t exist! ");
            }
        }
    }

    public void changeId(Scanner scanner) {
        sectionStore.setNewId(scanner);
    }

    public void changeName(Scanner scanner) {
        sectionStore.setNewName(scanner);
    }

    public void changeBudget(Scanner scanner) {
        sectionStore.setNewBudget(scanner);
    }

    public void changeBedsNumber(Scanner scanner) {
        sectionStore.setNewBedsNumber(scanner);
    }


}
