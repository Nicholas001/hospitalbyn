package com.nicholas.hospital.section;

import com.google.gson.Gson;
import com.nicholas.hospital.utils.Paths;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class SectionStore {

    private List<Section> allSections;
    private ReadSectionFromFile ReadSectionFromFile;
    private static SectionStore single_instance = null;

    private SectionStore() {
        this.ReadSectionFromFile = new ReadSectionFromFile();
        this.allSections = ReadSectionFromFile.readFromJsonFileUsingGson();
    }

    public static SectionStore getInstance() {
        if (single_instance == null) {
            single_instance = new SectionStore();
        }
        return single_instance;
    }


    public void saveSaction(Section section) {
        section.setId(generateIdForSaction());
        allSections.add(section);
    }

    public List<Section> findAllSections() {
        return allSections;
    }

    public Section findSectionById(int id) {
        for (Section section : allSections) {
            if (section.getId() == id) {
                return section;
            }
        }
        return null;
    }

    public void deleteAllSections() {
        allSections = new LinkedList<>();
    }

    public void deleteSectionById(int id) {
        for (Iterator<Section> iterator = allSections.iterator(); iterator.hasNext(); ) {
            Section section = iterator.next();
            if (section.getId() == id) {
                iterator.remove();
            }
        }
    }


    public void insertNewSectionFromConsole(Scanner scanner) {

        ReadSeactionFromConsole readSeactionFromConsole = new ReadSeactionFromConsole();
        saveSaction(readSeactionFromConsole.readSectionFromConsole(scanner));

    }


    public void saveAndExitUsingTxt() {
        try {
            FileWriter fileWriter = new FileWriter(Paths.PATH_TO_SECTIONS_TXT_FILE);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            for (int i = 0; i < allSections.size(); i++) {
                String lineText = String.valueOf(allSections.get(i).getId()) + " " + allSections.get(i).getName() + " " + String.valueOf(allSections.get(i).getBudget()) + " " +
                        String.valueOf(allSections.get(i).getBedNumber()) + '\n';
                bufferedWriter.write(lineText);
            }
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void saveAndExitUsingJson() {
        Gson gson = new Gson();
        try {
            FileWriter fileWriter = new FileWriter(Paths.PATH_TO_SECTIONS_JSON_FILE);
            fileWriter.write("[");

            for (Section each : allSections) {
                String text = gson.toJson(each);
                fileWriter.write(text);
                fileWriter.write(",");
            }
            fileWriter.write("]");
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private int generateIdForSaction() {
        int maxId = 0;
        for (Section section : allSections) {
            if (maxId < section.getId()) {
                maxId = section.getId();
            }
        }
        return maxId + 1;
    }

    public void setNewId(Scanner scanner) {
        System.out.println("Please insert Section ID ");
        int id = scanner.nextInt();
        System.out.println("Please insert the new ID ");
        int newId = scanner.nextInt();

        for (Section section : allSections) {
            if (section.getId() == id) {
                section.setId(newId);
                break;
            }
        }
        System.out.println("The new ID was set successfully");
        System.out.println();
    }

    public void setNewName(Scanner scanner) {
        System.out.println("Please insert Section ID ");
        int id = scanner.nextInt();
        System.out.println("Please insert the new name ");
        scanner.nextLine();
        String name = scanner.nextLine();

        for (Section section : allSections) {
            if (section.getId() == id) {
                section.setName(name);
            }
        }
        System.out.println("The new name was set successfully");
        System.out.println();
    }

    public void setNewBudget(Scanner scanner) {
        System.out.println("Please insert Section ID ");
        int id = scanner.nextInt();
        System.out.println("Please insert the new budget ");
        int newBudget = scanner.nextInt();

        for (Section section : allSections) {
            if (section.getId() == id) {
                section.setBudget(newBudget);
            }
        }
        System.out.println("The new budget was set successfully");
        System.out.println();
    }

    public void setNewBedsNumber(Scanner scanner) {
        System.out.println("Please insert Section ID ");
        int id = scanner.nextInt();
        System.out.println("Please insert the new beds number ");
        int newBedsNumber = scanner.nextInt();

        for (Section section : allSections) {
            if (section.getId() == id) {
                section.setBedNumber(newBedsNumber);
            }
        }
        System.out.println("The new beds number was set successfully");
        System.out.println();
    }
}
