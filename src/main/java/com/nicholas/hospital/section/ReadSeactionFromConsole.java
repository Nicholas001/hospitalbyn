package com.nicholas.hospital.section;

import java.util.Scanner;

public class ReadSeactionFromConsole {

    public Section readSectionFromConsole(Scanner scanner) {

        Section section = new Section();

        System.out.println("Please insert Section data: ");

        System.out.println("Please insert Section name");
        String name;
        scanner.nextLine();
        name = scanner.nextLine();


        System.out.println("Please insert Section budget ");
        int budget = scanner.nextInt();

        System.out.println("Please insert Section beds number");
        int bedNumber = scanner.nextInt();

        section.setBudget(budget);
        section.setName(name);
        section.setBedNumber(bedNumber);

        return section;

    }


}
